package dan.com.testplayer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.tvb.media.api.MediaAPI;
import com.tvb.media.constant.BundleKey;
import com.tvb.media.fragment.BaseVideoPlayerFragment;
import com.tvb.media.fragment.VideoPlayerFactory;
import com.tvb.media.view.controller.impl.TimeShiftPlayerUIController;

import java.io.File;

public class MainActivity extends AppCompatActivity implements BaseVideoPlayerFragment.TvbPlayerLifeCycleListener, BaseVideoPlayerFragment.TvbPlayerTrackingController {

    private BaseVideoPlayerFragment playerFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPlayer();
    }

    private void initPlayer() {
        MediaAPI.init(false, "api0.he.hk3.tvb.com");
        VideoPlayerFactory.init(VideoPlayerFactory.DeviceType.MOBILE);
        playerFragment = VideoPlayerFactory.getPlayerInstance(playerFragment, false, VideoPlayerFactory.DrmType.PLAIN, VideoPlayerFactory.QualityType.QUALITY_1080P, false, getBundle(), this, this);
        playerFragment.setViewVisibility(TimeShiftPlayerUIController.PlayerUIControllerViewEvent.CLOSE, View.VISIBLE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, playerFragment).commitAllowingStateLoss();
    }

    private Bundle getBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleKey.VIDEO_PATH, "http://evronovosti.mediacdn.ru/sr1/evronovosti/playlist.m3u8");
        return bundle;
    }

    @Override
    public void onTvbPlayerChangeStatus(BaseVideoPlayerFragment.PlayerStatus playerStatus, Object... objects) {

    }

    @Override
    public void onTvbPlayerError(BaseVideoPlayerFragment.PlayerStatus playerStatus, Object... objects) {

    }

    @Override
    public void onTvbPlayerShareToSocialNetwork(Object... objects) {

    }

    @Override
    public void onTvbPlayerAction(BaseVideoPlayerFragment.PlayerAction playerAction, Object... objects) {

    }

    @Override
    public void doTvbPlayerPageImpressionTracking(Object... objects) {

    }

    @Override
    public void doTvbPlayerClickEventTracking(Object... objects) {

    }

    @Override
    public void doTvbPlayerVideoTracking(Object... objects) {

    }

    @Override
    public void doTvbInStreamAdTracking(BaseVideoPlayerFragment.TrackingType trackingType, Object... objects) {

    }
}